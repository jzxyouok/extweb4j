Ext.define("Admin.model.system.log.Log",{
	extend: 'Admin.ux.Model',
    fields: [
        'id',
		'user_name',
		'log_url',
		'create_time',
		'log_params'
    ]
});