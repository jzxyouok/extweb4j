/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.5.47 : Database - extweb4j
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`extweb4j` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `extweb4j`;

/*Table structure for table `ext_dept` */

DROP TABLE IF EXISTS `ext_dept`;

CREATE TABLE `ext_dept` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `dept_name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  `dept_desc` varchar(500) DEFAULT NULL COMMENT '部门职能描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ext_dept` */

insert  into `ext_dept`(`id`,`dept_name`,`dept_desc`) values ('9986c2aec313412a85e1a01038e430c5','产品部','产品开发和维护'),('abb039b4e6ae4b8b8f5603743f3cf6ae','技术部','技术部门描述'),('de227cd134b0471ebf8f1a774f0b5111','运维组','专业维护'),('ed95178e03a34f139d378f5453eef5d4','信息中心','描述');

/*Table structure for table `ext_log` */

DROP TABLE IF EXISTS `ext_log`;

CREATE TABLE `ext_log` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `user_name` varchar(300) DEFAULT NULL COMMENT '日志标题',
  `log_url` varchar(500) DEFAULT NULL COMMENT '日志请求地址',
  `log_params` varchar(500) DEFAULT NULL COMMENT '日志参数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ext_log` */

insert  into `ext_log`(`id`,`user_name`,`log_url`,`log_params`,`create_time`) values ('1','zhsangan','/user/add','{name:zhangsan}','2016-06-10 01:05:14'),('2','list','/user/del','{name:zhangsan}','2016-06-10 01:05:14');

/*Table structure for table `ext_menu` */

DROP TABLE IF EXISTS `ext_menu`;

CREATE TABLE `ext_menu` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `text` varchar(50) DEFAULT NULL COMMENT '菜单标题',
  `leaf` char(1) DEFAULT '0' COMMENT '是否是叶子节点,0-不是,1-是',
  `pid` varchar(50) DEFAULT '0' COMMENT '父节点ID',
  `view_type` varchar(255) DEFAULT NULL COMMENT '渲染界面',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `icon_cls` varchar(100) DEFAULT NULL,
  `row_cls` varchar(100) DEFAULT NULL,
  `deep` char(11) DEFAULT '1' COMMENT '深度',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `action` varchar(255) DEFAULT '#' COMMENT '资源路径',
  `status` char(1) DEFAULT '1' COMMENT '1-启用,0-禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ext_menu` */

insert  into `ext_menu`(`id`,`text`,`leaf`,`pid`,`view_type`,`create_time`,`icon_cls`,`row_cls`,`deep`,`sort`,`action`,`status`) values ('020d9e228c0148ec808f6360153df917','统计','1','0','chart','2016-05-31 18:29:24','x-fa fa-line-chart','nav-tree-badge nav-tree-badge-new','1',8,NULL,'1'),('05a8e26fd0a047eea8fe796587ad6dd0','新增部门','1','b467700c293a4a549206b5438b93a771',NULL,'2016-06-27 10:27:03',NULL,NULL,'3',1,'/dept/add','1'),('10da6b9f2dc144ff9e4e05aca9de1c6a','系统日志','0','36634b3a36ae4ef0ac566432c98ed7a7','log-view','2016-05-30 20:45:04','x-fa fa-info',NULL,'2',4,NULL,'1'),('220b1e3fa78b4a7db0d213dc56700837','日志查询','1','10da6b9f2dc144ff9e4e05aca9de1c6a',NULL,'2016-06-02 08:54:01',NULL,NULL,'3',0,'/log/list','1'),('24f4bfc473d8438faa06de8369c7ab92','用户管理','0','36634b3a36ae4ef0ac566432c98ed7a7','user-view','2016-05-30 20:43:58','x-fa fa-user',NULL,'2',0,NULL,'1'),('36634b3a36ae4ef0ac566432c98ed7a7','系统管理','0','0',NULL,'2016-05-30 20:44:00','x-fa fa-gears',NULL,'1',1,NULL,'1'),('4aaa67985ebd423196e70f2a973ecc5d','编辑部门','1','b467700c293a4a549206b5438b93a771',NULL,'2016-06-27 10:27:39',NULL,NULL,'3',2,'/dept/edit','1'),('61e9f8e07e3449969fe7a9f849bac30a','角色管理','0','36634b3a36ae4ef0ac566432c98ed7a7','role-view','2016-05-30 20:43:58','x-fa fa-users',NULL,'2',1,NULL,'1'),('63e1cca5fae4488182080eb3f5e4c1c8','删除角色','1','61e9f8e07e3449969fe7a9f849bac30a',NULL,'2016-06-27 10:16:35',NULL,NULL,'3',3,'/role/delete','1'),('6a287b7360e6402580a4c90977c45bdc','主页','1','0','dashboard','2016-05-30 20:41:15','x-fa fa-desktop','nav-tree-badge nav-tree-badge-hot','1',0,NULL,'1'),('787a3559dca54c2b896c06b4d17f64fa','重置密码','1','24f4bfc473d8438faa06de8369c7ab92',NULL,'2016-06-27 09:42:41',NULL,NULL,'3',5,'/user/resetpwd','1'),('78a4bb7927c5459ba21af076465014a6','编辑用户','1','24f4bfc473d8438faa06de8369c7ab92',NULL,'2016-05-30 20:43:58',NULL,NULL,'3',2,'/user/edit','1'),('8cad5b44cc644edead242431c644f499','删除部门','1','b467700c293a4a549206b5438b93a771',NULL,'2016-06-27 10:28:02',NULL,NULL,'3',3,'/dept/delete','1'),('a0bda115335f4464bf34c75ef96c18af','编辑角色','1','61e9f8e07e3449969fe7a9f849bac30a',NULL,'2016-06-27 10:16:12',NULL,NULL,'3',2,'/role/edit','1'),('a98a684b73e24174b1718dd6495ad4b0','菜单&资源','1','36634b3a36ae4ef0ac566432c98ed7a7','tree-menu','2016-05-30 20:43:58','x-fa fa-navicon',NULL,'2',3,NULL,'1'),('b157d8a09742472bb02cc48ea784b18c','删除用户','1','24f4bfc473d8438faa06de8369c7ab92',NULL,'2016-06-08 13:07:30',NULL,NULL,'3',4,'/user/delete','1'),('b467700c293a4a549206b5438b93a771','部门管理','0','36634b3a36ae4ef0ac566432c98ed7a7','dept-view','2016-06-16 16:56:56','x-fa fa-graduation-cap',NULL,'2',2,NULL,'1'),('c089ed68fe6b47da85e5cb1abb6ec24d','创建角色','1','61e9f8e07e3449969fe7a9f849bac30a',NULL,'2016-06-27 10:15:52',NULL,NULL,'3',1,'/role/add','1'),('d08d362db39e43269fbd7f4ae6a464d0','部门列表','1','b467700c293a4a549206b5438b93a771',NULL,'2016-06-27 10:26:45',NULL,NULL,'3',0,'/dept/list','1'),('d442ead2915444eea92133d36dcf0a7a','分配权限','1','61e9f8e07e3449969fe7a9f849bac30a',NULL,'2016-06-27 10:17:26',NULL,NULL,'3',2,'/role/auth','1'),('d557c1f9fbe14c169a86186fa8be0bfb','查询用户','1','24f4bfc473d8438faa06de8369c7ab92',NULL,'2016-06-02 09:37:12',NULL,NULL,'3',0,'/user/list','1'),('df2defffefb7484bb07f0fc31543be4e','新增用户','1','24f4bfc473d8438faa06de8369c7ab92',NULL,'2016-05-30 20:43:58',NULL,NULL,'3',1,'/user/add','1'),('e396382bd397462596ae1a7f6bf3efc1','角色列表','1','61e9f8e07e3449969fe7a9f849bac30a',NULL,'2016-06-27 10:15:09',NULL,NULL,'3',0,'/role/list','1');

/*Table structure for table `ext_role` */

DROP TABLE IF EXISTS `ext_role`;

CREATE TABLE `ext_role` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `role_desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `row_status` char(11) DEFAULT '0' COMMENT '状态,0-启用，1-禁用，2-删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ext_role` */

insert  into `ext_role`(`id`,`role_name`,`create_time`,`role_desc`,`row_status`) values ('380dc2536c554eae9655ccc05f16374f','超级管理员','2016-06-08 14:40:54','拥有所有权限','0'),('6a287b7360e6402580a4c90977c45bdc','普通管理员','2016-06-08 21:47:04','拥有系统基本权限','2'),('6abdd6fe57694c4b992be3d5a554ca11','普通管理员','2016-06-11 11:39:16','普通管理员','0');

/*Table structure for table `ext_role_menu` */

DROP TABLE IF EXISTS `ext_role_menu`;

CREATE TABLE `ext_role_menu` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `menu_id` varchar(50) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ext_role_menu` */

insert  into `ext_role_menu`(`id`,`role_id`,`menu_id`) values ('326ddd5194cb4744a4b69c95bf24bdbf','380dc2536c554eae9655ccc05f16374f','e396382bd397462596ae1a7f6bf3efc1'),('34eb1230a5a145019752be007f3a4384','380dc2536c554eae9655ccc05f16374f','05a8e26fd0a047eea8fe796587ad6dd0'),('37fe0a2abc554c7e92ce28fef586d26b','380dc2536c554eae9655ccc05f16374f','df2defffefb7484bb07f0fc31543be4e'),('3f599cc93f2d40c7aa233377af9d0bcc','380dc2536c554eae9655ccc05f16374f','b467700c293a4a549206b5438b93a771'),('410b1439940941b9990f5228ebfc4acb','380dc2536c554eae9655ccc05f16374f','d442ead2915444eea92133d36dcf0a7a'),('540a55dd557a4aebb8c7be065fde1c5b','6abdd6fe57694c4b992be3d5a554ca11','6a287b7360e6402580a4c90977c45bdc'),('5f4718cfd8c0467bbb81fb84111e85e9','6abdd6fe57694c4b992be3d5a554ca11','020d9e228c0148ec808f6360153df917'),('5f5e249faa6c4234804eb6d7323c2b4d','380dc2536c554eae9655ccc05f16374f','36634b3a36ae4ef0ac566432c98ed7a7'),('635c3cb0e7364b49896142319af4af5b','380dc2536c554eae9655ccc05f16374f','a98a684b73e24174b1718dd6495ad4b0'),('6559b7fe546141848b2d6505d1e94ed7','6abdd6fe57694c4b992be3d5a554ca11','10da6b9f2dc144ff9e4e05aca9de1c6a'),('67960d5e947a4518a0849cafb096dff9','380dc2536c554eae9655ccc05f16374f','24f4bfc473d8438faa06de8369c7ab92'),('6a39530fdfdc4fd4bf260507d2d3c33b','380dc2536c554eae9655ccc05f16374f','10da6b9f2dc144ff9e4e05aca9de1c6a'),('6f2c2672bbe34cd79e2d0e68b1ebdf43','380dc2536c554eae9655ccc05f16374f','220b1e3fa78b4a7db0d213dc56700837'),('85278641a4f642dc8d342d318dd7fa1a','380dc2536c554eae9655ccc05f16374f','d08d362db39e43269fbd7f4ae6a464d0'),('8dc1f46ffde241e49d1866e294697279','380dc2536c554eae9655ccc05f16374f','8cad5b44cc644edead242431c644f499'),('8e1a96c3c527434bb14c9142c7e0fa15','380dc2536c554eae9655ccc05f16374f','4aaa67985ebd423196e70f2a973ecc5d'),('950a09f4d5d44c5abdcf845ae6d3543a','6abdd6fe57694c4b992be3d5a554ca11','36634b3a36ae4ef0ac566432c98ed7a7'),('a031bc2e4b3f43c7a47d6556f084b267','380dc2536c554eae9655ccc05f16374f','d557c1f9fbe14c169a86186fa8be0bfb'),('a3864da0d8d54107899b3c16c5e036de','380dc2536c554eae9655ccc05f16374f','a0bda115335f4464bf34c75ef96c18af'),('a5a6b3032a17423698d4df61431b44d4','380dc2536c554eae9655ccc05f16374f','78a4bb7927c5459ba21af076465014a6'),('a887f054d17e47ed83e41f2f36832f41','380dc2536c554eae9655ccc05f16374f','787a3559dca54c2b896c06b4d17f64fa'),('c74f34d44053434dbbb823d9905701cf','380dc2536c554eae9655ccc05f16374f','63e1cca5fae4488182080eb3f5e4c1c8'),('d0331aca64eb48f7b437a741c5e203d8','380dc2536c554eae9655ccc05f16374f','c089ed68fe6b47da85e5cb1abb6ec24d'),('d10c3af8db304953a2b8a72a3a56a0f7','380dc2536c554eae9655ccc05f16374f','b157d8a09742472bb02cc48ea784b18c'),('d56977e7ebfe47dcbe0f4b6e15b46f0d','380dc2536c554eae9655ccc05f16374f','61e9f8e07e3449969fe7a9f849bac30a'),('e331b6b7c7e146a4a9cbf81450f0d16d','380dc2536c554eae9655ccc05f16374f','6a287b7360e6402580a4c90977c45bdc'),('f9c4e6875a884794acd0931342dc6d0a','6abdd6fe57694c4b992be3d5a554ca11','220b1e3fa78b4a7db0d213dc56700837'),('fbb321fbc37545f182ae093fdfbdb257','380dc2536c554eae9655ccc05f16374f','020d9e228c0148ec808f6360153df917');

/*Table structure for table `ext_user` */

DROP TABLE IF EXISTS `ext_user`;

CREATE TABLE `ext_user` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户登录ID',
  `user_pwd` varchar(50) DEFAULT NULL COMMENT '用户密码',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户姓名',
  `user_email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `row_status` char(1) DEFAULT '0' COMMENT '状态,0-启用，1-禁用，2-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `user_desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `dept_id` varchar(50) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ext_user` */

insert  into `ext_user`(`id`,`user_id`,`user_pwd`,`user_name`,`user_email`,`row_status`,`create_time`,`user_desc`,`dept_id`) values ('b4e6e5259b7b4d56aafcd6b3073771db','zhangsan','E10ADC3949BA59ABBE56E057F20F883E','张三','zhangsan@vaco.com','0','2016-06-08 20:55:36','uuuuu','abb039b4e6ae4b8b8f5603743f3cf6ae'),('fa72ed734b7b48e1b09ceba1ffadb724','admin','E10ADC3949BA59ABBE56E057F20F883E','Admin管理员','admin@qq.com','0','2016-06-08 18:13:25','工作要肯拼，生活要欢乐。','9986c2aec313412a85e1a01038e430c5');

/*Table structure for table `ext_user_role` */

DROP TABLE IF EXISTS `ext_user_role`;

CREATE TABLE `ext_user_role` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户主键',
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色主键',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ext_user_role` */

insert  into `ext_user_role`(`id`,`user_id`,`role_id`) values ('0c555ffcf05645019ef27a6fb37cc77f','a84a2f53682645cd93adbc96c0c33f1e','6a287b7360e6402580a4c90977c45bdc'),('466b75401408467fa01e40b944e9e317','b4e6e5259b7b4d56aafcd6b3073771db','6abdd6fe57694c4b992be3d5a554ca11'),('87ec121412ab4a0cabd941228f498ddc','c46f6c7913374ade8565d9d412a385c1','6abdd6fe57694c4b992be3d5a554ca11'),('8da5a65835c346e3a2b8015780a88189','25b29869ef2d40f5a3ce8eaa589fea35','6abdd6fe57694c4b992be3d5a554ca11'),('99dd910cfa9e4142acefdae9bd98d2fc','7b7c4d46de6744f99a0031ed01f4346f','380dc2536c554eae9655ccc05f16374f'),('a039cd21a8494e3a9bfbe6ffa81d9140','c46f6c7913374ade8565d9d412a385c1','6a287b7360e6402580a4c90977c45bdc'),('a8bb350d28d547c597ba5c36dc3551ad','251b866b0b3e45b0b3979c02318289f7','6a287b7360e6402580a4c90977c45bdc'),('b8dc64d8f1634c8d9fc373cde52ad7e0','25b29869ef2d40f5a3ce8eaa589fea35','6a287b7360e6402580a4c90977c45bdc'),('dfd0986319e94742966f7c506083f4ff','34ddcbdcbe3c4b15b554680f176ad587','6abdd6fe57694c4b992be3d5a554ca11'),('fb058462cf6147ac9598e1cebbaa943a','fa72ed734b7b48e1b09ceba1ffadb724','380dc2536c554eae9655ccc05f16374f');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
