package com.extweb4j.web.controller;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import com.extweb4j.core.anno.AuthAnno;
import com.extweb4j.core.controller.ExtController;
import com.extweb4j.core.kit.DateKit;
import com.extweb4j.core.kit.ExtKit;
import com.extweb4j.core.model.ExtDept;
import com.extweb4j.core.render.PoiRender;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;
/**
 * 部门控制器
 * @author Administrator
 *
 */
public class DeptController extends ExtController{
	/**
	 * 列表
	 */
	@AuthAnno
	public void list(){
		int page = getParaToInt("page");
		int limit = getParaToInt("limit");
		String keywords = getPara("keywords");
		Page<ExtDept> pageData = ExtDept.dao.pageDataBy(page,limit,keywords);
		renderJson(pageData);
	}
	/**
	 * 新增
	 */
	@AuthAnno
	@Before(Tx.class)
	public void add(){
		ExtDept dept = getExtModel(ExtDept.class);
		dept.set("id",ExtKit.UUID());
		dept.save();
		success();
	}
	/**
	 * 编辑
	 */
	@AuthAnno
	@Before(Tx.class)
	public void edit(){
		ExtDept dept = getExtModel(ExtDept.class);
		dept.update();
		success();
	}
	/**
	 * 删除
	 */
	@AuthAnno
	@Before(Tx.class)
	public void delete(){
		ExtDept.dao.deleteById(getPara("id"));
		success();
	}
	
	/**
	 * 获取全部部门JSON
	 */
	public void json(){
		List<ExtDept> list = ExtDept.dao.findAll();
		renderJson(list);
	}
	/**
	 * 导出报表
	 * @throws UnsupportedEncodingException 
	 */
	public void export() throws UnsupportedEncodingException{
		
		List<ExtDept> list = ExtDept.dao.findAll();
		
		PoiRender render = new PoiRender(list);
		
		String[] columns = { "id","dept_name", "dept_desc", };
		String[] heades = { "ID","部门名称", "部门描述"};
		render(render
				.sheetName("部门报表")
				.cellWidth(4000)
				.headers(heades)
				.columns(columns)
				.fileName(new String("部门报表_".getBytes("gbk"), "ISO8859-1") +DateKit.format(new Date(), "yyyyMMdd") + ".xls"));
	}
}
